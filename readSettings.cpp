#include <QtCore/QDataStream>
#include <QtCore/QMetaType>
#include <QtCore/QFile>
#include <QtCore/QString>
#include <QtCore/QVariant>
#include <QtGui/QBrush>
#include <QtGui/QPen>
#include <iostream>
using std::cout;
using std::endl;
using std::string;

void printQVariantHash(QVariantHash settingsHash, char *spaces);
void printQVariant(QVariant qvariant, char *spaces);
void printQVariant(QVariant qvariant, const int size);
void printQBrushSize(QBrush brush, const int size);


void printQPenSize(QPen pen, const int size){
	Qt::PenStyle style = pen.style();
	int width = pen.width();
	QBrush brush = pen.brush();
	Qt::PenCapStyle capStyle = pen.capStyle();
	Qt::PenJoinStyle joinStyle = pen.joinStyle();
	char spaces[size+1];
	for(int i=0; i< size; i++){
		spaces[i] = ' ';
	}
	spaces[size]='\0';
	cout << spaces << "{" << endl;
	cout << spaces << "width: " << width << endl;
	cout << spaces << "brush: "  << endl;
	printQBrushSize(brush, size + 1);
	cout << spaces << "cap: " << capStyle << endl;
	cout << spaces << "join: " << joinStyle << endl;
	cout << spaces << "}" << endl;
}

void printGradient(const QGradient *gradient, const int size){
	char spaces[size+1];
	for(int i=0; i< size; i++){
		spaces[i] = ' ';
	}
	spaces[size]='\0';
	cout << spaces << "gradient: " << (gradient?"(gradient)":"null") << endl;
}

void printQBrushSize(QBrush brush, const int size){
	Qt::BrushStyle style = brush.style();
	QColor color = brush.color();
	const QGradient *gradient = brush.gradient();
	
	char spaces[size+1];
	for(int i=0; i< size; i++){
		spaces[i] = ' ';
	}
	spaces[size]='\0';
	cout << spaces << "{" << endl;
	cout << spaces << "style: " << style << endl;
	cout << spaces << "color: " << QVariant(color).toString().toStdString() << endl;
	printGradient(gradient, size);
	cout << spaces << "}" << endl;
}

void printQVariantListSize(QVariantList settingsList, const int size){
	char spaces[size+1];
	for(int i=0; i< size; i++){
		spaces[i] = ' ';
	}
	spaces[size]='\0';
	QVariantList::iterator i = settingsList.begin();
	cout << spaces << "List-Size: " << settingsList.size() << endl;
	while(i != settingsList.end()){
		printQVariant(*i, spaces);
		++i;
	}
}

void printQVariantHashSize(QVariantHash settingsHash, const int size){
	char spaces[size+1];
	for(int i=0; i< size; i++){
		spaces[i] = ' ';
	}
	spaces[size]='\0';
	printQVariantHash(settingsHash, spaces);
}

void printQVariantHash(QVariantHash settingsHash, char *spaces){
	QHash<QString, QVariant>::iterator i = settingsHash.begin();
	cout << spaces << "Hash-Size: " << settingsHash.size() << endl;
	while(i != settingsHash.end()){
		cout << spaces << i.key().toStdString();
		printQVariant(*i, strlen(spaces));
		++i;
	}
}

void printQVariant(QVariant qvariant, const int size){
	char spaces[size+1];
	for(int i=0; i< size; i++){
		spaces[i] = ' ';
	}
	spaces[size]='\0';
	printQVariant(qvariant, spaces);
}

void printQVariant(QVariant qvariant, char *spaces){
	int userType = qvariant.userType();
	const char *name = QVariant::typeToName(userType);
	cout << spaces << "(" << (name?name:"null") << '/' << userType << "): ";
	switch(userType){
		case QMetaType::QVariantList:
		{
			cout << endl;
			QList<QVariant> subsettingsList = qvariant.value<QVariantList>();
			printQVariantListSize(subsettingsList, strlen(spaces) + 1);
			break;
		}
		case QMetaType::QVariantHash:
		{
			cout << endl;
			QHash<QString, QVariant> subsettingsHash = qvariant.value<QVariantHash>();
			printQVariantHashSize(subsettingsHash, strlen(spaces) + 1);
			break;
		}
		case QMetaType::QBrush:
		{
			cout << endl;
			QBrush brush = qvariant.value<QBrush>();
			printQBrushSize(brush, strlen(spaces) + 1);
			break;
		}
		case QMetaType::QPen:
		{
			cout << endl;
			QPen pen = qvariant.value<QPen>();
			printQPenSize(pen, strlen(spaces) + 1);
			break;
		}
		default:
		{
			cout << qvariant.toString().toStdString() << endl;
			break;
		}
	}
}

int main(int argc, char **argv){
	QString filename(argv[1]);
	QFile infile(filename);
	infile.open(QIODevice::ReadOnly);
	QByteArray data = infile.readAll();
	infile.close();
	QDataStream instream(data);
	instream.setVersion(QDataStream::Qt_4_5);
	quint16 checksum;
	qint32 settingsVersion;
	qint32 dataStreamVersion;
	instream >> checksum;
	instream >> settingsVersion;
	cout << "Settings-Version: " << settingsVersion << endl;
	instream >> dataStreamVersion;
	QByteArray data2;
	instream >> data2;
	QByteArray fromB64 = QByteArray::fromBase64(data2);

	QDataStream s(&fromB64,QIODevice::ReadWrite);
	s.setVersion( dataStreamVersion );
	QHash<QString, QVariant> settingsHash= QHash<QString, QVariant>();
	s >> settingsHash;
	printQVariantHashSize(settingsHash, 0);
	return 0;
}
