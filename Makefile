QT_VERSION=5
#g++ does not work for some reason!?
#CC = g++
CC = clang++
include scorep-install.make-include

run: readSettings
	@./readSettings trace.vsettings

show-spark: TRACE_PATH=trace-spark.vsettings
show-spark: show

show-flink: TRACE_PATH=trace-flink.vsettings
show-flink: show

show:
	./readSettings ${TRACE_PATH}

run-writer-flink: TRACE_PATH=traces-flink.otf2
run-writer-flink: writeSettings
	LD_LIBRARY_PATH=$$LD_LIBRARY_PATH:${SCOREP_PATH}/lib ./writeSettings ${TRACE_PATH} flink

run-writer: TRACE_PATH=trace-spark.otf2
run-writer: writeSettings
	LD_LIBRARY_PATH=$$LD_LIBRARY_PATH:${SCOREP_PATH}/lib ./writeSettings ${TRACE_PATH}

readSettings: readSettings.cpp Makefile
	${CC} -g -O0 -I/usr/include/qt${QT_VERSION} -I/usr/include/x86_64-linux-gnu/qt${QT_VERSION} $< -o$@ -fPIC -lQt${QT_VERSION}Gui -lQt${QT_VERSION}Core

writeSettings: writeSettings.cpp Makefile
	${CC} -g -O0 -I${SCOREP_PATH}/include -I/usr/include/qt${QT_VERSION} -I/usr/include/x86_64-linux-gnu/qt${QT_VERSION} $< -o$@ -fPIC -lQt${QT_VERSION}Gui -lQt${QT_VERSION}Core -L${SCOREP_PATH}/lib -lotf2
