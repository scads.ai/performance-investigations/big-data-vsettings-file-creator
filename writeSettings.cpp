#include <QtCore/QDataStream>
#include <QtCore/QFile>
#include <QtCore/QString>
#include <QtCore/QVariant>
#include <QtGui/QPen>
#include <iostream>
#include <otf2/otf2.h>
#include <sstream>
#include <string.h>

#define Q_DATA_STREAM_VERSION QDataStream::Qt_4_5

struct regioncolor{
	const char *name;
	const char *brushColor;
};

std::string asReplacedOtf2WithVsettingsEnding(char *otf2FilePath){
	if(NULL == otf2FilePath){
		return NULL;
	}
	const char *suffix = ".otf2";
	size_t str_len = strlen(otf2FilePath);
	size_t suffix_len = strlen(suffix);

	if(suffix_len > str_len){
		return NULL;
	}

	if(0 == strncmp( otf2FilePath + str_len - suffix_len, suffix, suffix_len )){
		std::string orig(otf2FilePath);
		orig.replace(orig.end()-suffix_len, orig.end(), ".vsettings");
		return orig;
	}
	return NULL;
}

std::string getTraceId(char *otf2FilePath){
	uint64_t trace_id;
	OTF2_Reader *reader = OTF2_Reader_Open(otf2FilePath);
	OTF2_Reader_GetTraceId( reader, &trace_id );
	std::ostringstream id;
	id << std::hex << trace_id;
	return id.str();
}

int main(int argc, char **argv){
	QHash<QString, QVariant> outerHash = QHash<QString, QVariant>();
	QHash<QString, QVariant> settingsHash = QHash<QString, QVariant>();
	QHash<QString, QVariant> functionAppearance = QHash<QString, QVariant>();
	QHash<QString, QVariant> schemas = QHash<QString, QVariant>();
	QHash<QString, QVariant> defaultHash = QHash<QString, QVariant>();
	QHash<QString, QVariant> saveState = QHash<QString, QVariant>();
	QHash<QString, QVariant> stateGroups = QHash<QString, QVariant>();
	int colorCount;
	struct regioncolor *colors;
#define arrsize(arr) ((sizeof arr)/(sizeof (arr[0])))
	if(argc > 2 && strcmp("flink", argv[2]) == 0){
		struct regioncolor flinkcolors[]={
			{"SCHEDULED", "#0000ff"},
			{"RUNNING", "#aaff00"},
			{"DEPLOYING", "#ffaa00"},
			{"Vertice", "#ff0000"},
			{"CREATED", "#000000"},
			{"INITIALIZING", "#ffff00"}
		};
		colorCount = arrsize(flinkcolors);
		colors = flinkcolors;

		QHash<QString, QVariant> functionFilter = QHash<QString, QVariant>();
		QHash<QString, QVariant> filterItems = QHash<QString, QVariant>();
		filterItems["activeFilter"] = 0;
		QList<QVariant> filterList = QList<QVariant>();
		QHash<QString, QVariant> filter = QHash<QString, QVariant>();
		filter["type"] = 0;
		QList<QVariant> ruleList = QList<QVariant>();
		QHash<QString, QVariant> rule = QHash<QString, QVariant>();
		rule["ruleTest"] = 9;
		QHash<QString, QVariant> names = QHash<QString, QVariant>();
		names["CREATED"] = 1;
		names["SCHEDULED"] = 2;
		rule["ruleData"] = names;
		rule["ruleType"] = 201;
		rule["hasConflict"] = false;
		ruleList.append(rule);
		filter["rules"] = ruleList;
		filter["description"] = QString("Filter");
		filterList.append(filter);
		filterItems["filters"] = filterList;
		functionFilter[QString::fromStdString(getTraceId(argv[1]))] = filterItems;
		settingsHash["FunctionFilters"] = functionFilter;
	}else{
		struct regioncolor sparkcolors[]={
			{"SCHEDULER_DELAY", "#0000ff"},
			{"TASK_DESERIALIZATION", "#ffaa00"},
			{"RUNNING", "#aaff00"},
			{"Stage", "#ff0000"},
		};
		colorCount = arrsize(sparkcolors);
		colors = sparkcolors;
#undef arrsize
	}
	for (int i = 0; i < colorCount; i++){
		QHash<QString, QVariant> state= QHash<QString, QVariant>();
		QHash<QString, QVariant> colorData= QHash<QString, QVariant>();
		
		colorData["fillBrush"] = QColor(QString(colors[i].brushColor));
		state["saveState"] = colorData;
		stateGroups[QString(colors[i].name)] = state;
	}
	saveState["stateGroups"] = stateGroups;
	defaultHash["saveState"] = saveState;
	schemas["Default"] = defaultHash;
	functionAppearance["schemas"] = schemas;
	functionAppearance["currentSchema"] = QString("Default");
	settingsHash["FunctionAppearance"] = functionAppearance;
	outerHash["settings"] = settingsHash;
	//
	QByteArray data;
	QDataStream settings(&data, QIODevice::WriteOnly);
	settings.setVersion(Q_DATA_STREAM_VERSION);
	settings << outerHash;
	data = data.toBase64();

	// create the checksum as the last item of work 
	quint16 checkSum = qChecksum(data.constData(), data.size());

	QByteArray out_data;
	QDataStream out_stream2(&out_data, QIODevice::WriteOnly);
	const quint32 dataStreamVersion = Q_DATA_STREAM_VERSION;
	out_stream2.setVersion(dataStreamVersion);

	// checksum has to be in front of anything else
	out_stream2 << checkSum;
	const qint32 settingsVersion = 2;
	out_stream2 << settingsVersion;
	out_stream2 << dataStreamVersion;
	out_stream2 << data;

	QString filename(QString::fromStdString(asReplacedOtf2WithVsettingsEnding(argv[1])));
	QFile outfile(filename);
	outfile.open(QIODevice::WriteOnly);
	outfile.write(out_data);
	outfile.close();
	return 0;
}
