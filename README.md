# Big Data VSettings File Creator

This project aims to provide a .vsettings file for traces generated with the scripts from https://gitlab.hrz.tu-chemnitz.de/scads.ai/performance-investigations/big-data-trace-from-history, so that [Vampir](https://tu-dresden.de/zih/forschung/projekte/vampir?set_language=en) opens with the right colors.

## Installation
To use the programs, you need to have Qt 5 and clang++ and either Score-P or OTF2 installed. Then, create a file `scorep-install.make-include` with the following content:

```make
SCOREP_PATH = /path/to/your/score-p/installation
```

This file is automatically used when you invoke make.

## Running the programs

To run the programs, you need to directly indicate a trace. The vsettings file is then generated into the same directory. The following should be sufficient to create and execute the programs:

```bash
#for Apache Spark
make run-writer TRACE_PATH=/tmp/spark-trace-from-history/app-20220801111802-0000/traces.otf2

#for Apache Flink
make run-writer-flink TRACE_PATH=/tmp/flink-trace-from-history/2308e81eda6cf5e63ce054b2a427aa31/traces.otf2
```

## Support
The hope is that this README.md is self-explanatory. Please open issues if you feel that an improvement is necessary.

## Contributing
Contributions are very welcome! Feel free to share ideas by opening issues or contributing code.

## License
Please see [the License](LICENSE).

## Project status
This project is active, but the maintainer has a lot of other things to do. Thus, improvements (bug fixes, new features) only appear from time to time. So, please contribute!
